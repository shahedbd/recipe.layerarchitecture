﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Recipe.Admin.Startup))]
namespace Recipe.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
