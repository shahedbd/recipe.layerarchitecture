﻿app.service('CRUDService', function ($http) {
    var CRUDService = {};
    this.GetAll = function (apiRoute) { return $http.get(apiRoute); }
    this.Save = function (apiRoute, entity) { return $http.post(apiRoute, entity); }
    this.Update = function (apiRoute, entity) { return $http.put(apiRoute, entity) }
    this.Delete = function (apiRoute) { return $http.delete(apiRoute); }
    this.GetOthersByID = function (apiRoute) { return $http.get(apiRoute); }
});