﻿
app.controller('RecipesCtrl', ['$scope', 'CRUDService', 'uiGridConstants',
    function ($scope, CRUDService, uiGridConstants) {

        $scope.gridOptions = [];

        //Pagination
        $scope.pagination = {
            paginationPageSizes: [15, 25, 50, 75, 100, "All"],
            ddlpageSize: 15,
            pageNumber: 1,
            pageSize: 15,
            totalItems: 0,

            getTotalPages: function () {
                return Math.ceil(this.totalItems / this.pageSize);
            },
            pageSizeChange: function () {
                if (this.ddlpageSize == "All")
                    this.pageSize = $scope.pagination.totalItems;
                else
                    this.pageSize = this.ddlpageSize

                this.pageNumber = 1
                $scope.GetRecipes();
            },
            firstPage: function () {
                if (this.pageNumber > 1) {
                    this.pageNumber = 1
                    $scope.GetRecipes();
                }
            },
            nextPage: function () {
                if (this.pageNumber < this.getTotalPages()) {
                    this.pageNumber++;
                    $scope.GetRecipes();
                }
            },
            previousPage: function () {
                if (this.pageNumber > 1) {
                    this.pageNumber--;
                    $scope.GetRecipes();
                }
            },
            lastPage: function () {
                if (this.pageNumber >= 1) {
                    this.pageNumber = this.getTotalPages();
                    $scope.GetRecipes();
                }
            }
        };


        //ui-Grid Call
        $scope.GetRecipes = function () {
            $scope.loaderMore = true;
            $scope.lblMessage = 'loading please wait....!';
            $scope.result = "color-green";

            $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
                if (col.filters[0].term) {
                    return 'header-filtered';
                } else {
                    return '';
                }
            };
            var removeTemplate = '<button class="btn btn-danger" ng-click="grid.appScope.removeRow(row)"><i class="glyphicon glyphicon-remove"></i></button>';
            $scope.gridOptions = {
                useExternalPagination: true,
                useExternalSorting: true,
                enableFiltering: true,
                enableSorting: true,
                enableRowSelection: true,
                enableSelectAll: true,
                enableGridMenu: true,

                columnDefs: [
                    { name: "ID", displayName: "ID", width: '10%', headerCellClass: $scope.highlightFilteredHeader },
                    { name: "Category", title: "Category", width: '10%', headerCellClass: $scope.highlightFilteredHeader },
                    { name: "Name", title: "Name", headerCellClass: $scope.highlightFilteredHeader },
                    { name: "Author", title: "Author", headerCellClass: $scope.highlightFilteredHeader },
                    { name: "Ingredients", title: "Ingredients", headerCellClass: $scope.highlightFilteredHeader },



                    {
                        name: 'Delete',
                        enableFiltering: false,
                        enableSorting: false,
                        width: '10%',
                        enableColumnResizing: false,
                        cellTemplate: removeTemplate
                    }

                    //{ name: "CreatedOn", displayName: "Created On", cellFilter: 'date:"short"', headerCellClass: $scope.highlightFilteredHeader },

                    //{
                    //    name: 'Edit',
                    //    enableFiltering: false,
                    //    enableSorting: false,
                    //    width: '5%',
                    //    enableColumnResizing: false,
                    //    cellTemplate: '<span class="label label-warning label-mini">' +
                    //                  '<a href="" style="color:white" title="Select" ng-click="grid.appScope.GetByID(row.entity)">' +
                    //                    '<i class="fa fa-check-square" aria-hidden="true"></i>' +
                    //                  '</a>' +
                    //                  '</span>'
                    //}
                ],

                exporterAllDataFn: function () {
                    return getPage(1, $scope.gridOptions.totalItems, paginationOptions.sort)
                    .then(function () {
                        $scope.gridOptions.useExternalPagination = false;
                        $scope.gridOptions.useExternalSorting = false;
                        getPage = null;
                    });
                },
            };

            var NextPage = (($scope.pagination.pageNumber - 1) * $scope.pagination.pageSize);
            var NextPageSize = $scope.pagination.pageSize;
            var apiRoute = '../api/recipes/GetByRecipes/' + NextPage + '/' + NextPageSize;
            var result = CRUDService.GetAll(apiRoute);
            result.then(
                function (response) {
                    $scope.pagination.totalItems = response.data.recordsTotal;
                    $scope.gridOptions.data = response.data.recipesList;
                    $scope.loaderMore = false;
                },
            function (error) {
                console.log("Error: " + error);
            });
        }


        //Default Load
        $scope.GetRecipes();


        //Selected Call
        $scope.GetByID = function (model) {
            $scope.SelectedRow = model;
        };
    }
]);