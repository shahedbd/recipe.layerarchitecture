﻿using Recipe.BL.Admin;
using Recipe.Common.Log;
using Recipe.DBModels.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Recipe.Admin.Controllers.ServiceController
{
    [RoutePrefix("api/recipes")]
    public class RecipesController : BaseApiController
    {
        private readonly IRecipesBL recipesBL = null;

        public RecipesController(ILogger _logger, IRecipesBL _recipesBL)
        {
            Logger = _logger;
            recipesBL = _recipesBL; //new HolidayBL(Logger);
        }

        /// <summary>
        /// Get All Recipes List
        /// </summary>
        /// <returns>Recipes List</returns>
        // GET: api/Recipes
        [HttpGet]
        [Route("GetAll")]
        public IHttpActionResult GetAll()
        {
            try
            {
                var holidayList = recipesBL.GetAllRecipes();
                if (holidayList == null)
                {
                    return NotFound();
                }
                return Ok(holidayList);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }




        [HttpGet, ResponseType(typeof(Recipes)), Route("GetByRecipes/{pageNumber:int}/{pageSize:int}")]
        public async Task<IHttpActionResult> GetByRecipes(int pageNumber, int pageSize)
        {
            IEnumerable<Recipes> recipesList = null;
            int recordsTotal = 0;
            try
            {
                recipesList = await recipesBL.GetAllRecipes();

                recordsTotal = recipesList.Count();
                recipesList = recipesList.OrderBy(x => x.ID)
                                     .Skip(pageNumber)
                                     .Take(pageSize)
                                     .ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return await Task.FromResult(Json(new { recordsTotal, recipesList }));
        }





        /// <summary>
        /// GetById Recipes
        /// </summary>
        /// <returns> Recipes list by id</returns>
        // GET: /api/Recipes/GetById
        [Route("GetById/{id:long}")]
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                var result = recipesBL.GetRecipesByID(id);
                if (result == null)
                    return NotFound();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Insert Recipes
        /// </summary>
        /// <param name="Recipes"></param>
        /// <returns>HttpResponseMessage</returns>
        // POST: api/Recipes
        [HttpPost]
        [Route("InsertRecipes")]
        public HttpResponseMessage InsertRecipes(Recipes Recipes)
        {
            HttpResponseMessage response = null;
            try
            {
                dynamic data = recipesBL.InsertRecipes(Recipes);
                response = data != null ? Request.CreateResponse(HttpStatusCode.Created, "Recipes saved successfully") : Request.CreateResponse(HttpStatusCode.InternalServerError, "Not created");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return response;
        }
        /// <summary>
        /// Update Recipes
        /// </summary>
        /// <param name="Recipes"></param>
        /// <returns>HttpResponseMessage</returns>
        [Route("UpdateRecipes")]
        // PUT: api/Recipes/5
        public HttpResponseMessage UpdateRecipes(Recipes Recipes)
        {
            HttpResponseMessage response = null;
            try
            {
                var result = recipesBL.UpdateRecipes(Recipes);
                response = result != null ? Request.CreateResponse(HttpStatusCode.Created, "Recipes updated successfully") : Request.CreateResponse(HttpStatusCode.InternalServerError, "Not updated");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return response;
        }

        /// <summary>
        /// Delete Recipes
        /// </summary>
        /// <param name="id">long SL</param>
        /// <returns> Delete Recipes by id</returns>
        // GET: /api/Recipes/DeleteHoliday
        [Route("DeleteRecipes/{id:long}")]
        [HttpGet]
        public HttpResponseMessage DeleteRecipes(int id)
        {
            HttpResponseMessage response = null;
            try
            {
                var result = recipesBL.DeleteRecipes(id);
                response = result != null ? Request.CreateResponse(HttpStatusCode.Created, "Recipes deleted successfully") : Request.CreateResponse(HttpStatusCode.InternalServerError, "Not deleted");
            }
            catch (Exception ex)
            {

                response = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return response;
        }


    }
}
