﻿using Recipe.Common.Log;
using System.Web.Http;

namespace Recipe.Admin.Controllers
{
    public class BaseApiController : ApiController
    {
        protected ILogger Logger { get; set; }
    }
}
