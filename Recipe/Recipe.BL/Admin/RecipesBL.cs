using Recipe.Common.Log;
using Recipe.Data.Implementations;
using Recipe.DBModels.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Recipe.BL.Admin
{


    public class RecipesBL : IRecipesBL
    {

        protected ILogger Logger { get; set; }

        public RecipesBL(ILogger logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Insert Recipes
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Message</returns>
        public Task<string> InsertRecipes(Recipes entity)
        {
            try
            {
                var result = new RecipesRepository(Logger).Insert(entity);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Update Recipes
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Message</returns>
        public Task<string> UpdateRecipes(Recipes entity)
        {
            try
            {
                var result = new RecipesRepository(Logger).Update(entity);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Delete Recipes
        /// </summary>
        /// <param name="ID"></param>
        /// <returns>Message</returns>
        public Task<string> DeleteRecipes(int ID)
        {
            try
            {
                var result = new RecipesRepository(Logger).Delete(ID);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Get All Recipes
        /// </summary>
        /// <returns>List ofRecipes</returns>
        public Task<IEnumerable<Recipes>> GetAllRecipes()
        {
            try
            {
                var result = new RecipesRepository(Logger).GetAll();
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Get Recipes by ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns>Recipes Object</returns>
        public Task<Recipes> GetRecipesByID(int ID)
        {
            try
            {
                var result = new RecipesRepository(Logger).GetRecipesByID(ID);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }
        }

    }


}
