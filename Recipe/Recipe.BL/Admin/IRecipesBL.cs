using Recipe.DBModels.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Recipe.BL.Admin
{
    public interface IRecipesBL
    {
        Task<string> InsertRecipes(Recipes entity);
        Task<string> UpdateRecipes(Recipes entity);
        Task<string> DeleteRecipes(int ID);
        Task<IEnumerable<Recipes>> GetAllRecipes();
        Task<Recipes> GetRecipesByID(int ID);
    }
}
