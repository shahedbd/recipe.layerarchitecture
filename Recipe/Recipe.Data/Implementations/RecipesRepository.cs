using Recipe.Common.Log;
using Recipe.Data.Repositories;
using Recipe.DBModels.Model;
using Recipe.Infrastructure.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;


namespace Recipe.Data.Implementations
{
    public class RecipesRepository : Repository<Recipes>, IRecipesRepository<Recipes>
    {

        protected ILogger Logger { get; set; }

        public RecipesRepository(ILogger logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Insert Recipes
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Message</returns>
        public async Task<string> Insert(Recipes entity)
        {
            try
            {
                var cmd = new SqlCommand("sp_Recipes");
                cmd.Parameters.AddWithValue("@ID", entity.ID);
                cmd.Parameters.AddWithValue("@CAT_ID", entity.CAT_ID);
                cmd.Parameters.AddWithValue("@Category", entity.Category);
                cmd.Parameters.AddWithValue("@Name", entity.Name);
                cmd.Parameters.AddWithValue("@Author", entity.Author);
                cmd.Parameters.AddWithValue("@Ingredients", entity.Ingredients);
                cmd.Parameters.AddWithValue("@Instructions", entity.Instructions);
                cmd.Parameters.AddWithValue("@Date", entity.Date);
                cmd.Parameters.AddWithValue("@HOMEPAGE", entity.HOMEPAGE);
                cmd.Parameters.AddWithValue("@LINK_APPROVED", entity.LINK_APPROVED);
                cmd.Parameters.AddWithValue("@HITS", entity.HITS);
                cmd.Parameters.AddWithValue("@RATING", entity.RATING);
                cmd.Parameters.AddWithValue("@NO_RATES", entity.NO_RATES);
                cmd.Parameters.AddWithValue("@TOTAL_COMMENTS", entity.TOTAL_COMMENTS);
                cmd.Parameters.AddWithValue("@HIT_DATE", entity.HIT_DATE);
                cmd.Parameters.AddWithValue("@RecipeImage", entity.RecipeImage);
                cmd.Parameters.AddWithValue("@upsize_ts", entity.upsize_ts);

                cmd.Parameters.Add("@Msg", SqlDbType.NChar, 500);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@pOptions", 1);

                var result = await ExecuteNonQueryProc(cmd);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Update Recipes
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Message</returns>
        public async Task<string> Update(Recipes entity)
        {
            try
            {
                var cmd = new SqlCommand("sp_Recipes");
                cmd.Parameters.AddWithValue("@ID", entity.ID);
                cmd.Parameters.AddWithValue("@CAT_ID", entity.CAT_ID);
                cmd.Parameters.AddWithValue("@Category", entity.Category);
                cmd.Parameters.AddWithValue("@Name", entity.Name);
                cmd.Parameters.AddWithValue("@Author", entity.Author);
                cmd.Parameters.AddWithValue("@Ingredients", entity.Ingredients);
                cmd.Parameters.AddWithValue("@Instructions", entity.Instructions);
                cmd.Parameters.AddWithValue("@Date", entity.Date);
                cmd.Parameters.AddWithValue("@HOMEPAGE", entity.HOMEPAGE);
                cmd.Parameters.AddWithValue("@LINK_APPROVED", entity.LINK_APPROVED);
                cmd.Parameters.AddWithValue("@HITS", entity.HITS);
                cmd.Parameters.AddWithValue("@RATING", entity.RATING);
                cmd.Parameters.AddWithValue("@NO_RATES", entity.NO_RATES);
                cmd.Parameters.AddWithValue("@TOTAL_COMMENTS", entity.TOTAL_COMMENTS);
                cmd.Parameters.AddWithValue("@HIT_DATE", entity.HIT_DATE);
                cmd.Parameters.AddWithValue("@RecipeImage", entity.RecipeImage);
                cmd.Parameters.AddWithValue("@upsize_ts", entity.upsize_ts);

                cmd.Parameters.Add("@Msg", SqlDbType.NChar, 500);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@pOptions", 2);

                var result = await ExecuteNonQueryProc(cmd);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Delete Recipes
        /// </summary>
        /// <param name="ID"></param>
        /// <returns>Message</returns>
        public async Task<string> Delete(int ID)
        {
            try
            {
                var cmd = new SqlCommand("sp_Recipes");
                cmd.Parameters.AddWithValue("@ID", ID);
                cmd.Parameters.Add("@Msg", SqlDbType.NChar, 500);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@pOptions", 3);

                var result = await ExecuteNonQueryProc(cmd);
                //if (Convert.ToString(result).Trim().Contains("Data Deleted Successfully"))
                //{
                //    new LiveLogHistoryRepository(logger).Insert(ID.ToString() + " has been Deleted.", 1, 3);
                //}
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Get All Recipes
        /// </summary>
        /// <returns>List ofRecipes</returns>
        public async Task<IEnumerable<Recipes>> GetAll()
        {
            try
            {
                var cmd = new SqlCommand("sp_Recipes");
                cmd.Parameters.AddWithValue("@pOptions", 4);
                var result = await GetDataReaderProc(cmd);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Get Recipes by ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns>Recipes Object</returns>
        public async Task<Recipes> GetRecipesByID(int ID)
        {
            try
            {
                var cmd = new SqlCommand("sp_Recipes");
                cmd.Parameters.AddWithValue("@ID", ID);
                cmd.Parameters.AddWithValue("@pOptions", 5);
                Recipes result = await GetByDataReaderProc(cmd);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Data Mapping for Recipes
        /// </summary>
        /// <param name="sqldatareader"></param>
        /// <returns>Recipes Object</returns>
        public Recipes Mapping(SqlDataReader reader)
        {
            try
            {
                Recipes oRecipes = new Recipes();
                oRecipes.ID = Helper.ColumnExists(reader, "ID") ? ((reader["ID"] == DBNull.Value) ? 0 : Convert.ToInt32(reader["ID"])) : 0;
                oRecipes.CAT_ID = Helper.ColumnExists(reader, "CAT_ID") ? ((reader["CAT_ID"] == DBNull.Value) ? 0 : Convert.ToInt32(reader["CAT_ID"])) : 0;
                oRecipes.Category = Helper.ColumnExists(reader, "Category") ? reader["Category"].ToString() : "";
                oRecipes.Name = Helper.ColumnExists(reader, "Name") ? reader["Name"].ToString() : "";
                oRecipes.Author = Helper.ColumnExists(reader, "Author") ? reader["Author"].ToString() : "";
                oRecipes.Ingredients = Helper.ColumnExists(reader, "Ingredients") ? reader["Ingredients"].ToString() : "";
                oRecipes.Instructions = Helper.ColumnExists(reader, "Instructions") ? reader["Instructions"].ToString() : "";
                oRecipes.Date = Helper.ColumnExists(reader, "Date") ? ((reader["Date"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(reader["Date"])) : Convert.ToDateTime("01/01/1900");
                oRecipes.HOMEPAGE = Helper.ColumnExists(reader, "HOMEPAGE") ? reader["HOMEPAGE"].ToString() : "";
                oRecipes.LINK_APPROVED = Helper.ColumnExists(reader, "LINK_APPROVED") ? ((reader["LINK_APPROVED"] == DBNull.Value) ? 0 : Convert.ToInt32(reader["LINK_APPROVED"])) : 0;
                oRecipes.HITS = Helper.ColumnExists(reader, "HITS") ? ((reader["HITS"] == DBNull.Value) ? 0 : Convert.ToInt32(reader["HITS"])) : 0;
                oRecipes.RATING = Helper.ColumnExists(reader, "RATING") ? ((reader["RATING"] == DBNull.Value) ? 0 : Convert.ToInt32(reader["RATING"])) : 0;
                oRecipes.NO_RATES = Helper.ColumnExists(reader, "NO_RATES") ? ((reader["NO_RATES"] == DBNull.Value) ? 0 : Convert.ToInt32(reader["NO_RATES"])) : 0;
                oRecipes.TOTAL_COMMENTS = Helper.ColumnExists(reader, "TOTAL_COMMENTS") ? ((reader["TOTAL_COMMENTS"] == DBNull.Value) ? 0 : Convert.ToInt32(reader["TOTAL_COMMENTS"])) : 0;
                oRecipes.HIT_DATE = Helper.ColumnExists(reader, "HIT_DATE") ? ((reader["HIT_DATE"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(reader["HIT_DATE"])) : Convert.ToDateTime("01/01/1900");
                oRecipes.RecipeImage = Helper.ColumnExists(reader, "RecipeImage") ? reader["RecipeImage"].ToString() : "";
                oRecipes.upsize_ts = Helper.ColumnExists(reader, "upsize_ts") ? ((reader["upsize_ts"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(reader["upsize_ts"])) : Convert.ToDateTime("01/01/1900");
                return oRecipes;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }
        }

        public Task<Recipes> GetByID(long ID)
        {
            throw new NotImplementedException();
        }

        public Task<string> Delete(long ID)
        {
            throw new NotImplementedException();
        }
    }


}
