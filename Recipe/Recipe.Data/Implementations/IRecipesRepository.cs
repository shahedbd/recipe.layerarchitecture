﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Recipe.Data.Repositories
{
    interface IRecipesRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetByID(long ID);
        Task<string> Insert(T Entity);
        Task<string> Delete(long ID);
        Task<string> Update(T Entity);
        T Mapping(SqlDataReader sqldatareader);
    }
}
