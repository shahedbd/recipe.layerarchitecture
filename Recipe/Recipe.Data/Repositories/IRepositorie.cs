﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Recipe.Data.Repositories
{
    public interface IRepository<T> where T : class
    {
        //Execute Inline Query
        Task<SqlDataReader> GetDataReader(string strText);
        Task<int> ExecuteText(string strText);
        Task<dynamic> GetResultByExecuteText(string strText);




        //Execute Stored Procedure
        Task<string> ExecuteNonQueryProc(SqlCommand cmd);
        Task<IEnumerable<T>> GetDataReaderProc(SqlCommand cmd);
        Task<T> GetByDataReaderProc(SqlCommand cmd);


        dynamic PopulateRecord(SqlDataReader sqldatareader, string TableName);

    }
}
