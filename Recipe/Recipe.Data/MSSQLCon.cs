﻿using System;
using System.Configuration;
using System.Data.SqlClient;


namespace Recipe.Data
{
    public static class MSSQLConn
    {
        public static SqlConnection MSSQLConnection()
        {
            SqlConnection conn = null;
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["RecipesConLocal"].ConnectionString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return conn;
        }
    }
}
