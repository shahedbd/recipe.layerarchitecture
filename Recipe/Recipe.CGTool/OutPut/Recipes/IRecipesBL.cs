using FXTF.CRM.Model.Model.Admin;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FXTF.CRM.Service.Admin.Interfaces
{
    public interface IRecipesBL
    {
Task<string> InsertRecipes(Recipes entity);
Task<string> UpdateRecipes(Recipes entity);
Task<string> DeleteRecipes(Recipes entity);
Task<IEnumerable<Recipes>> GetAllRecipes();
Task<Recipes> GetRecipesByID(int ID)
    }
}
