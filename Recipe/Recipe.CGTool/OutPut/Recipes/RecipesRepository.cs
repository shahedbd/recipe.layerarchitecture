using FXTF.CRM.Common.Log;
using FXTF.CRM.Common.Utility;
using FXTF.CRM.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using FXTF.CRM.Data.Repositories.Interfaces;
using FXTF.CRM.Model.Model.Admin;

namespace FXTF.CRM.Data.Repositories.Implementations
{


public class RecipesRepository : DBOperations<Recipes>, IRepository<Recipes>
{

protected ILogger Logger { get; set; }

public RecipesRepository(ILogger logger)
{
Logger = logger;
}

/// <summary>
/// Insert Recipes
/// </summary>
/// <param name="entity"></param>
/// <returns>Message</returns>
public async Task<string> Insert(Recipes entity)
{
try
{
var cmd = new SqlCommand("sp_Recipes");
cmd.Parameters.AddWithValue("@ID", entity.ID);
cmd.Parameters.AddWithValue("@CAT_ID", entity.CAT_ID);
cmd.Parameters.AddWithValue("@Category", entity.Category);
cmd.Parameters.AddWithValue("@Name", entity.Name);
cmd.Parameters.AddWithValue("@Author", entity.Author);
cmd.Parameters.AddWithValue("@Ingredients", entity.Ingredients);
cmd.Parameters.AddWithValue("@Instructions", entity.Instructions);
cmd.Parameters.AddWithValue("@Date", entity.Date);
cmd.Parameters.AddWithValue("@HOMEPAGE", entity.HOMEPAGE);
cmd.Parameters.AddWithValue("@LINK_APPROVED", entity.LINK_APPROVED);
cmd.Parameters.AddWithValue("@HITS", entity.HITS);
cmd.Parameters.AddWithValue("@RATING", entity.RATING);
cmd.Parameters.AddWithValue("@NO_RATES", entity.NO_RATES);
cmd.Parameters.AddWithValue("@TOTAL_COMMENTS", entity.TOTAL_COMMENTS);
cmd.Parameters.AddWithValue("@HIT_DATE", entity.HIT_DATE);
cmd.Parameters.AddWithValue("@RecipeImage", entity.RecipeImage);
cmd.Parameters.AddWithValue("@upsize_ts", entity.upsize_ts);

cmd.Parameters.Add("@Msg", SqlDbType.NChar, 500);
cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
cmd.Parameters.AddWithValue("@pOptions", 1);

var result = await ExecuteNonQueryProc(cmd);
return result;
}
catch (Exception ex)
{
Logger.Error(ex.Message);
throw ex;
}
}

/// <summary>
/// Update Recipes
/// </summary>
/// <param name="entity"></param>
/// <returns>Message</returns>
public async Task<string> Update(Recipes entity)
{
try
{
var cmd = new SqlCommand("sp_Recipes");
cmd.Parameters.AddWithValue("@ID", entity.ID);
cmd.Parameters.AddWithValue("@CAT_ID", entity.CAT_ID);
cmd.Parameters.AddWithValue("@Category", entity.Category);
cmd.Parameters.AddWithValue("@Name", entity.Name);
cmd.Parameters.AddWithValue("@Author", entity.Author);
cmd.Parameters.AddWithValue("@Ingredients", entity.Ingredients);
cmd.Parameters.AddWithValue("@Instructions", entity.Instructions);
cmd.Parameters.AddWithValue("@Date", entity.Date);
cmd.Parameters.AddWithValue("@HOMEPAGE", entity.HOMEPAGE);
cmd.Parameters.AddWithValue("@LINK_APPROVED", entity.LINK_APPROVED);
cmd.Parameters.AddWithValue("@HITS", entity.HITS);
cmd.Parameters.AddWithValue("@RATING", entity.RATING);
cmd.Parameters.AddWithValue("@NO_RATES", entity.NO_RATES);
cmd.Parameters.AddWithValue("@TOTAL_COMMENTS", entity.TOTAL_COMMENTS);
cmd.Parameters.AddWithValue("@HIT_DATE", entity.HIT_DATE);
cmd.Parameters.AddWithValue("@RecipeImage", entity.RecipeImage);
cmd.Parameters.AddWithValue("@upsize_ts", entity.upsize_ts);

cmd.Parameters.Add("@Msg", SqlDbType.NChar, 500);
cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
cmd.Parameters.AddWithValue("@pOptions", 2);

var result = await ExecuteNonQueryProc(cmd);
return result;
}
catch (Exception ex)
{
Logger.Error(ex.Message);
throw ex;
}
}

/// <summary>
/// Delete Recipes
/// </summary>
/// <param name="ID"></param>
/// <returns>Message</returns>
public async Task<string> Delete(int ID)
{
try
{
var cmd = new SqlCommand("sp_Recipes");
cmd.Parameters.AddWithValue("@ID", ID);
cmd.Parameters.Add("@Msg", SqlDbType.NChar, 500);


cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
cmd.Parameters.AddWithValue("@pOptions", 3);


var result = await ExecuteNonQueryProc(cmd);
if (Convert.ToString(result).Trim().Contains("Data Deleted Successfully"))
{
new LiveLogHistoryRepository(logger).Insert(ID.ToString() + " has been Deleted.", 1, 3);
}
return result;
}
catch (Exception ex)
{
Logger.Error(ex.Message);
throw ex;
}
}

/// <summary>
/// Get All Recipes
/// </summary>
/// <returns>List ofRecipes</returns>
public async Task<IEnumerable<Recipes>> GetAll()
{
try
{
var cmd = new SqlCommand("sp_Recipes");
cmd.Parameters.AddWithValue("@pOptions", 4);
var result = await GetDataReaderProc(cmd);
return result;
}
catch (Exception ex)
{
Logger.Error(ex.Message);
throw ex;
}
}

/// <summary>
/// Get Recipes by ID
/// </summary>
/// <param name="ID"></param>
/// <returns>Recipes Object</returns>
public async Task<Recipes> GetRecipesByID(int ID)
{
try
{
var cmd = new SqlCommand("sp_Recipes");
cmd.Parameters.AddWithValue("@ID", ID);
cmd.Parameters.AddWithValue("@pOptions", 5);
var result = await GetDataReaderProc(cmd);
return result;
}
catch (Exception ex)
{
Logger.Error(ex.Message);
throw ex;
}
}

/// <summary>
/// Data Mapping for Recipes
/// </summary>
/// <param name="sqldatareader"></param>
/// <returns>Recipes Object</returns>
public Recipes Mapping(SqlDataReader sqldatareader)
{
try
{
Recipes oRecipes = new Recipes();
oRecipes.ID = ColumnExists(reader, "ID") ? ((reader["ID"] == DBNull.Value) ? 0 : Convert.ToInt32(reader["ID"])) : 0 ;
oRecipes.CAT_ID = ColumnExists(reader, "CAT_ID") ? ((reader["CAT_ID"] == DBNull.Value) ? 0 : Convert.ToInt32(reader["CAT_ID"])) : 0 ;
oRecipes.Category = ColumnExists(reader, "Category") ? reader["Category"].ToString() : "";
oRecipes.Name = ColumnExists(reader, "Name") ? reader["Name"].ToString() : "";
oRecipes.Author = ColumnExists(reader, "Author") ? reader["Author"].ToString() : "";
oRecipes.Ingredients = ColumnExists(reader, "Ingredients") ? reader["Ingredients"].ToString() : "";
oRecipes.Instructions = ColumnExists(reader, "Instructions") ? reader["Instructions"].ToString() : "";
oRecipes.Date = ColumnExists(reader, "Date") ? ((reader["Date"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(reader["Date"])) : Convert.ToDateTime("01/01/1900");
oRecipes.HOMEPAGE = ColumnExists(reader, "HOMEPAGE") ? reader["HOMEPAGE"].ToString() : "";
oRecipes.LINK_APPROVED = ColumnExists(reader, "LINK_APPROVED") ? ((reader["LINK_APPROVED"] == DBNull.Value) ? 0 : Convert.ToInt32(reader["LINK_APPROVED"])) : 0 ;
oRecipes.HITS = ColumnExists(reader, "HITS") ? ((reader["HITS"] == DBNull.Value) ? 0 : Convert.ToInt32(reader["HITS"])) : 0 ;
oRecipes.RATING = ColumnExists(reader, "RATING") ? ((reader["RATING"] == DBNull.Value) ? 0 : Convert.ToInt32(reader["RATING"])) : 0 ;
oRecipes.NO_RATES = ColumnExists(reader, "NO_RATES") ? ((reader["NO_RATES"] == DBNull.Value) ? 0 : Convert.ToInt32(reader["NO_RATES"])) : 0 ;
oRecipes.TOTAL_COMMENTS = ColumnExists(reader, "TOTAL_COMMENTS") ? ((reader["TOTAL_COMMENTS"] == DBNull.Value) ? 0 : Convert.ToInt32(reader["TOTAL_COMMENTS"])) : 0 ;
oRecipes.HIT_DATE = ColumnExists(reader, "HIT_DATE") ? ((reader["HIT_DATE"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(reader["HIT_DATE"])) : Convert.ToDateTime("01/01/1900");
oRecipes.RecipeImage = 
oRecipes.upsize_ts = 
return oRecipes;
}
catch (Exception ex)
{
Logger.Error(ex.Message);
throw ex;
}
}


}


}
