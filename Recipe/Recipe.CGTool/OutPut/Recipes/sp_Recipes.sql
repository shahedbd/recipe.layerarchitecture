SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  proc [dbo].[sp_Recipes]
(
@ID		int = null,
@CAT_ID		int = null,
@Category		nvarchar(50) = null,
@Name		nvarchar(150) = null,
@Author		nvarchar(50) = null,
@Ingredients		ntext = null,
@Instructions		ntext = null,
@Date		datetime = null,
@HOMEPAGE		nvarchar(100) = null,
@LINK_APPROVED		int = null,
@HITS		int = null,
@RATING		int = null,
@NO_RATES		int = null,
@TOTAL_COMMENTS		int = null,
@HIT_DATE		datetime = null,
@RecipeImage		varchar = null,
@upsize_ts		timestamp = null,

@Msg				nvarchar(MAX)=null OUT ,
@pOptions			int 
)
AS
--Save Recipes
if(@pOptions=1)
begin
INSERT INTO Recipes
(
ID,
CAT_ID,
Category,
Name,
Author,
Ingredients,
Instructions,
Date,
HOMEPAGE,
LINK_APPROVED,
HITS,
RATING,
NO_RATES,
TOTAL_COMMENTS,
HIT_DATE,
RecipeImage,
upsize_ts

)
VALUES
(	
@ID,
@CAT_ID,
@Category,
@Name,
@Author,
@Ingredients,
@Instructions,
@Date,
@HOMEPAGE,
@LINK_APPROVED,
@HITS,
@RATING,
@NO_RATES,
@TOTAL_COMMENTS,
@HIT_DATE,
@RecipeImage,
@upsize_ts

)
IF @@ROWCOUNT = 0
Begin
SET @Msg='Warning: No rows were Inserted';	
End
Else
Begin
SET @Msg='Data Saved Successfully';	
End					
end
--End of Save Recipes



--Update Recipes 
if(@pOptions=2)
begin
UPDATE	Recipes 
SET
CAT_ID	=	@CAT_ID ,
Category	=	@Category ,
Name	=	@Name ,
Author	=	@Author ,
Ingredients	=	@Ingredients ,
Instructions	=	@Instructions ,
Date	=	@Date ,
HOMEPAGE	=	@HOMEPAGE ,
LINK_APPROVED	=	@LINK_APPROVED ,
HITS	=	@HITS ,
RATING	=	@RATING ,
NO_RATES	=	@NO_RATES ,
TOTAL_COMMENTS	=	@TOTAL_COMMENTS ,
HIT_DATE	=	@HIT_DATE ,
RecipeImage	=	@RecipeImage ,
upsize_ts	=	@upsize_ts 




WHERE	ID	=	@ID;



IF @@ROWCOUNT = 0
Begin
SET @Msg='Warning: No rows were Updated';	
End
Else
Begin
SET @Msg='Data Updated Successfully';
End
End
--End of Update Recipes 



--Delete Recipes



if(@pOptions=3)
begin
Delete from Recipes Where ID=@ID;
SET @Msg='Data Deleted Successfully';
end



--End of Delete Recipes 



--Select All Recipes 



if(@pOptions=4)
begin	        
select * from Recipes;
if(@@ROWCOUNT=0)
SET @Msg='Data Not Found';
end



--End of Select All Recipes 



--Select Recipes By ID 
if(@pOptions=5)
begin
select * from Recipes Where ID=@ID;



if(@@ROWCOUNT=0)
SET @Msg='Data Not Found';
end
--End of Select Recipes By ID 
