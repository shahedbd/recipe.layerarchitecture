using System;
namespace FXTF.CRM.Model.Model.Admin
{
    public class Recipes
    {
      public int ID { get; set; }
      public int CAT_ID { get; set; }
      public string Category { get; set; }
      public string Name { get; set; }
      public string Author { get; set; }
      public string Ingredients { get; set; }
      public string Instructions { get; set; }
      public DateTime Date { get; set; }
      public string HOMEPAGE { get; set; }
      public int LINK_APPROVED { get; set; }
      public int HITS { get; set; }
      public int RATING { get; set; }
      public int NO_RATES { get; set; }
      public int TOTAL_COMMENTS { get; set; }
      public DateTime HIT_DATE { get; set; }
      public  RecipeImage { get; set; }
      public  upsize_ts { get; set; }
    }
}
