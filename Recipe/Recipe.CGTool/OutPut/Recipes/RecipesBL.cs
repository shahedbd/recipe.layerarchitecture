using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FXTF.CRM.Common.Log;
using FXTF.CRM.Service.Admin.Interfaces;
using FXTF.CRM.Data.Repositories.Implementations;
using FXTF.CRM.Model.Model.Admin;

namespace FXTF.CRM.Service.Admin.Implementations
{


public class RecipesBL : IRecipesBL
{

protected ILogger Logger { get; set; }

public RecipesBL(ILogger logger)
{
Logger = logger;
}

/// <summary>
/// Insert Recipes
/// </summary>
/// <param name="entity"></param>
/// <returns>Message</returns>
public async Task<string> Insert(Recipes entity)
{
try
{
var result = await new RecipesRepository(logger).Insert(entity);
return result;
}
catch (Exception ex)
{
Logger.Error(ex.Message);
throw ex;
}
}

/// <summary>
/// Update Recipes
/// </summary>
/// <param name="entity"></param>
/// <returns>Message</returns>
public async Task<string> Update(Recipes entity)
{
try
{
var result = await new HoliDayRepository(logger).Update(entity);
return result;
}
catch (Exception ex)
{
Logger.Error(ex.Message);
throw ex;
}
}

/// <summary>
/// Delete Recipes
/// </summary>
/// <param name="ID"></param>
/// <returns>Message</returns>
public async Task<string> Delete(int ID)
{
try
{
var result = await new RecipesRepository(logger).Delete(ID);
return result;
}
catch (Exception ex)
{
Logger.Error(ex.Message);
throw ex;
}
}

/// <summary>
/// Get All Recipes
/// </summary>
/// <returns>List ofRecipes</returns>
public async Task<IEnumerable<Recipes>> GetAll()
{
try
{
var result = await new RecipesRepository(logger).GetAll();
return result;
}
catch (Exception ex)
{
Logger.Error(ex.Message);
throw ex;
}
}

/// <summary>
/// Get Recipes by ID
/// </summary>
/// <param name="ID"></param>
/// <returns>Recipes Object</returns>
public async Task<Recipes> GetRecipesByID(int ID)
{
try
{
var result = await new RecipesRepository(logger).GetRecipesByID(ID);
return result;
}
catch (Exception ex)
{
Logger.Error(ex.Message);
throw ex;
}
}


}


}
