﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/21/2008>
-- Description:	<Get the newest and top recipes for the sidemenu with count date difference.>
-- =============================================
CREATE PROCEDURE [dbo].[GetNewestRecipesSideMenu] 

@CatId int = '',
@Top int

AS
BEGIN

SET ROWCOUNT @Top

If (@CatId = '')

    Begin

       -- Return all 10 newest recipes.
       SELECT ID,
                     Name,
                     HITS,
                     Category,
                     Date 
       FROM Recipes 
       Where LINK_APPROVED = 1
       Order By Date DESC

    End

Else

Begin

       -- Return 15 newest recipes based on selected category.
       SELECT ID,
                     Name,
                     HITS,
                     Category,
                     Date
       FROM Recipes 
       Where CAT_ID = @CatId
       AND LINK_APPROVED = 1 
       Order By Date DESC

End

    -- never forget to set it back to 0! 
    SET ROWCOUNT 0 

END