﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/28/2008>
-- Description:	<Add recipe>
-- =============================================
CREATE PROCEDURE [dbo].[AddRecipe]

@Name varchar(50),
@Author varchar(20),
@Cat_Id int,
@Category varchar(50),
@Ingredients varchar(1000),
@Instructions varchar(1000),
@RecipeImage varchar(50) = Null

AS

declare @ErrorCode int

set @ErrorCode = @@error

BEGIN TRANSACTION

if ( @ErrorCode = 0 )

Begin

If (@RecipeImage = '' OR @RecipeImage = NULL)
	Begin
		INSERT INTO Recipes (Name,Author,CAT_ID,Category,Ingredients,Instructions, RecipeImage)
			VALUES(
				@Name,
				@Author,
				@Cat_Id,
				@Category,
				@Ingredients,
				@Instructions,
				NULL
				)
	End

Else

	Begin

		INSERT INTO Recipes (Name,Author,CAT_ID,Category,Ingredients,Instructions, RecipeImage)
			VALUES(
				@Name,
				@Author,
				@Cat_Id,
				@Category,
				@Ingredients,
				@Instructions,
				@RecipeImage
				)

	End

End

if ( @ErrorCode = 0 )
	COMMIT TRANSACTION
else
	ROLLBACK TRANSACTION

return @ErrorCode