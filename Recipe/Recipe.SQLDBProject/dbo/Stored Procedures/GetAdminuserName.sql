﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/29/2008>
-- Description:	<Returns admin username for validation>
-- =============================================
CREATE PROCEDURE [dbo].[GetAdminuserName]

@Username varchar(50)

AS
BEGIN

	SET NOCOUNT ON;

	SELECT uname from users where uname = @Username

END