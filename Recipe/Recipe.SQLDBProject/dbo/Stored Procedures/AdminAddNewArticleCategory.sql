﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <7/4/2008>
-- Description:	<Add new article category>
-- =============================================
CREATE PROCEDURE AdminAddNewArticleCategory 

@CatName varchar(100)

AS

declare @ErrorCode int

set @ErrorCode = @@error

BEGIN TRANSACTION

if ( @ErrorCode = 0 )

Begin

-- Insert
INSERT INTO Cooking_Article_Category (CAT_NAME)
	VALUES(@CatName)

End

if ( @ErrorCode = 0 )
	COMMIT TRANSACTION
else
	ROLLBACK TRANSACTION

return @ErrorCode