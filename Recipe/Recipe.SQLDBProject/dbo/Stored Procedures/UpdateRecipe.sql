﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <6/12/2008>
-- Description:	<Update Recipe>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateRecipe] 

@ID int,
@Name varchar(50),
@Author varchar(20),
@Cat_Id int,
@Ingredients varchar(1000),
@Instructions varchar(1000),
@Hits int,
@RecipeImage varchar(50) = Null

AS

DECLARE @ErrorCode int
DECLARE @Image varchar(50)
DECLARE @CatName varchar(50)

BEGIN

IF (@RecipeImage = '')
    BEGIN 
        SET @Image = NULL
     END
ELSE
    BEGIN
        SET @Image = @RecipeImage
    END

IF (@Cat_Id = 0)

	BEGIN

	UPDATE Recipes SET 
				   Name = @Name,
				   Author = @Author,
				   Ingredients = @Ingredients,
				   Instructions = @Instructions,
				   HITS = @Hits,
				   RecipeImage = @Image
				   WHERE ID = @ID
	SET @ErrorCode = @@error

	END

ELSE

	BEGIN

	--Get Category Name
	SELECT @CatName = CAT_TYPE From RECIPE_CAT WHERE CAT_ID = @Cat_Id

	UPDATE Recipes SET 
				   Name = @Name,
				   Author = @Author,
				   CAT_ID = @Cat_Id,
				   Category = @CatName,
				   Ingredients = @Ingredients,
				   Instructions = @Instructions,
				   HITS = @Hits,
				   RecipeImage = @Image
				   WHERE ID = @ID
	SET @ErrorCode = @@error

	END
		
END

RETURN @ErrorCode