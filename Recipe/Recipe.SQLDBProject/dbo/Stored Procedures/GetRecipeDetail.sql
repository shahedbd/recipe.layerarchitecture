﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/23/2008>
-- Description:	<Return recipe details>
-- =============================================
CREATE PROCEDURE [dbo].[GetRecipeDetail]

@ID int

AS
BEGIN

	SET NOCOUNT ON;

-- Get recipe
Select ID, 
       CAT_ID, 
       Category,
       Date,
       Name,  
       HITS, 
       NO_RATES,
       Ingredients,
       Instructions,
       Author,
       TOTAL_COMMENTS,
       LINK_APPROVED,
       RecipeImage, 
       CAST((1.0 * RATING/NO_RATES) as decimal(2,1)) as Rates 
       FROM Recipes 
       WHERE LINK_APPROVED = 1 AND ID = @ID;

       --Update hits
       Update Recipes set HITS = HITS + 1  where ID = @ID;

       --Update hit_date - use display todays 100 viewed recipes and last 4 hours
       Update Recipes SET HIT_DATE = getdate() WHERE ID = @ID

END