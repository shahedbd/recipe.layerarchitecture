﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/28/2008>
-- Description:	<Return 20 newest RSS XML>
-- =============================================
CREATE PROCEDURE GetXMLNewestRecipe 

AS
BEGIN

	SET NOCOUNT ON;

SELECT Top 20 ID,Name,HITS, Date,Category FROM Recipes Where LINK_APPROVED = 1 Order By Date DESC

END