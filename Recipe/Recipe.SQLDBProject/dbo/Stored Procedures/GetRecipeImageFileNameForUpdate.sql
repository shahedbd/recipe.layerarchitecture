﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <9/7/2008>
-- Description:	<Get image filename for update>
-- =============================================
CREATE PROCEDURE GetRecipeImageFileNameForUpdate 

@ID int

AS
BEGIN

	SET NOCOUNT ON;

    SELECT RecipeImage FROM Recipes WHERE ID = @ID
END