﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <6/6/2008>
-- Description:	<Return Article Category with count.>
-- =============================================
CREATE PROCEDURE GetArticleCategoryListing

AS
BEGIN

	SET NOCOUNT ON;

   SELECT *,(SELECT COUNT (*)  
     FROM Cooking_Article WHERE Cooking_Article.CAT_ID = Cooking_Article_Category.CAT_ID 
     AND Show = 1) AS REC_COUNT 
     FROM Cooking_Article_Category
     ORDER BY CAT_NAME ASC

END