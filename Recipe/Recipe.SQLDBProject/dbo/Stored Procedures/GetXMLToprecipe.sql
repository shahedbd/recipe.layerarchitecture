﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/28/2008>
-- Description:	<Return Top 20 RSS>
-- =============================================
CREATE PROCEDURE GetXMLToprecipe

AS
BEGIN

	SET NOCOUNT ON;

SELECT Top 20 ID,Name,HITS, Date,Category FROM Recipes Where LINK_APPROVED = 1 Order By Hits DESC

END