﻿-- =============================================
-- Author:		<Dexter,,Zafra>
-- Create date: <5/17/2008>
-- Description:	<Get the 15 lates recipe for the pageview.aspx page.>
-- =============================================
CREATE PROCEDURE PageView_GetNewestRecipeSideMenu 

AS
BEGIN

	SET NOCOUNT ON;

SELECT TOP 15 ID,Name,HITS,Category FROM Recipes Where LINK_APPROVED = 1 Order By Date DESC

END