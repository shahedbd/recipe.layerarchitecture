﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/23/2008>
-- Description:	<Add comments>
-- =============================================
CREATE PROCEDURE [dbo].[AddComments]

@ID int,
@Author varchar(20),
@Email varchar(50),
@Comments varchar(200)

AS

declare @ErrorCode int

set @ErrorCode = @@error

BEGIN TRANSACTION

if ( @ErrorCode = 0 )

Begin

-- Insert
INSERT INTO COMMENTS_RECIPE (ID,AUTHOR,EMAIL,COMMENTS)
	VALUES(
		@ID,
		@Author,
		@Email,
		@Comments
		);

-- Update comment count
Update Recipes SET TOTAL_COMMENTS = TOTAL_COMMENTS + 1 where ID = @ID

End

if ( @ErrorCode = 0 )
	COMMIT TRANSACTION
else
	ROLLBACK TRANSACTION

return @ErrorCode