﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/29/2008>
-- Description:	<Returns waiting for approval recipe count.>
-- =============================================
CREATE PROCEDURE GetUnApprovedRecipeCount

AS
BEGIN

	SET NOCOUNT ON;

    Select Count(*) From Recipes Where LINK_APPROVED = 0

END