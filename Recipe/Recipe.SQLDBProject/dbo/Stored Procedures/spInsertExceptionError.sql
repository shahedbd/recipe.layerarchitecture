﻿
CREATE PROCEDURE spInsertExceptionError

@URL varchar(100),
@Exception varchar(1000)

AS

SET NOCOUNT ON;

INSERT INTO dbo.ExceptionLog (URL, Exception)
VALUES (@URL, @Exception)

Return 0