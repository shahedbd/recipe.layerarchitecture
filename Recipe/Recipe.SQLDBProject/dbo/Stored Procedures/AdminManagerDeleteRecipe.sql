﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/29/2008>
-- Description:	<Perform delete from Admin recipe manager>
-- =============================================
CREATE PROCEDURE AdminManagerDeleteRecipe

@ID int

AS
BEGIN

	SET NOCOUNT ON;

   Delete Recipes where ID = @ID

END