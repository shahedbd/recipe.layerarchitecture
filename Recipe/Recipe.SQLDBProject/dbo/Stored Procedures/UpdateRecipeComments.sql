﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <7/3/2008>
-- Description:	<Update recipe comment>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateRecipeComments] 

@ID int,
@Author varchar(50),
@Email varchar(50),
@Comment varchar(255)

AS

declare @ErrorCode int

Begin

Update COMMENTS_RECIPE Set 
                       AUTHOR = @Author,
                       EMAIL = @Email,
                       COMMENTS = @Comment
                       Where COM_ID = @ID

set @ErrorCode = @@error
		
End

return @ErrorCode