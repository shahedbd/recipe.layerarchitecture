﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <7/4/2008>
-- Description:	<Delete recipe category>
-- =============================================
CREATE PROCEDURE AdminDeleteRecipeCategory 

@CatId int

AS
BEGIN

	SET NOCOUNT ON;

    -- Category table
    Delete From RECIPE_CAT Where CAT_ID = @CatId;

    -- Recipe table, delete all associated recipes
    Delete From Recipes Where CAT_ID = @CatId

END