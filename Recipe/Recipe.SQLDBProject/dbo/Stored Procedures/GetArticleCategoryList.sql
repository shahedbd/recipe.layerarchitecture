﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <6/5/2008>
-- Description:	<Returns article category list>
-- =============================================
CREATE PROCEDURE GetArticleCategoryList 

AS
BEGIN

	SET NOCOUNT ON;

     SELECT *,(SELECT COUNT (*)  
     FROM Cooking_Article WHERE Cooking_Article.CAT_ID = Cooking_Article_Category.CAT_ID 
     AND Show = 1) AS REC_COUNT 
     FROM Cooking_Article_Category
     ORDER BY CAT_NAME ASC

END