﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <7/3/2008>
-- Description:	<Delete Comment>
-- =============================================
CREATE PROCEDURE [dbo].[AdminDeleteRecipeComments] 

@COMID int,
@RecipeID int

AS
BEGIN

	SET NOCOUNT ON;

    Delete COMMENTS_RECIPE Where COM_ID = @COMID;

    -- Decrement Total comments count in recipe tabale
    Update Recipes Set TOTAL_COMMENTS = TOTAL_COMMENTS - 1 where ID = @RecipeID

END