﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/29/2008>
-- Description:	<Return admin password for validation>
-- =============================================
CREATE PROCEDURE [dbo].[GetAdminPassword]

@Password varchar(50)

AS
BEGIN

	SET NOCOUNT ON;


	SELECT password from users where password = @Password
END