﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/23/2008>
-- Description:	<Return related recipes in the recipedetail page.>
-- =============================================
CREATE PROCEDURE [dbo].[GetRelatedRecipe]

@CatId int

AS
BEGIN

	SET NOCOUNT ON;

	SELECT TOP 15 ID,
           CAT_ID,
           Name,
           HITS,
           Category
           From Recipes
           Where LINK_APPROVED = 1
           AND CAT_ID = @CatId
           Order By HITS DESC
END