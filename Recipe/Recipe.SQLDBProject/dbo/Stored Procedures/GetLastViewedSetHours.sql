﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/31/2008>
-- Description:	<Return last viewed hours>
-- =============================================
CREATE PROCEDURE GetLastViewedSetHours

AS
BEGIN

	SET NOCOUNT ON;

    SELECT (Hourspan / 60) as MinuteSpan FROM ConfigureLastViewedHours

END