﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/23/2008>
-- Description:	<Returns homepage ethnic category>
-- =============================================
CREATE PROCEDURE [dbo].[GetHomepageEthnicRegionalCategory]

AS
BEGIN

	SET NOCOUNT ON;

     SELECT *,(SELECT COUNT (*)  
     FROM Recipes WHERE Recipes.CAT_ID = RECIPE_CAT.CAT_ID 
     AND LINK_APPROVED = 1) AS REC_COUNT 
     FROM RECIPE_CAT WHERE GROUPCAT = 2
     ORDER BY CAT_TYPE ASC

END