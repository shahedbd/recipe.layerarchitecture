﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/29/2008>
-- Description:	<Returns last viewed>
-- =============================================
CREATE PROCEDURE [dbo].[GetLastViewedRecipe]

AS
BEGIN

	SET NOCOUNT ON;

    -- Use sub-query to get the datediff
Declare @HourSpan int

Select @HourSpan = HourSpan From ConfigureLastViewedHours

Select Top 15 ID, 
       Name, 
       Category, 
       HITS, 
       HIT_DATE,
       TotalTime / 3600 As Hours, 
       (TotalTime % 3600) / 60 As Minutes, 
       TotalTime % 60 as Seconds,
       (@HourSpan / 60) as MinuteSpan
    From
      (
	    Select ID, 
               Name, 
               Category, 
               HITS, 
               HIT_DATE, 
               DateDiff(second, HIT_DATE, GetDate()) As TotalTime 
	           From Recipes  
               Where DateDiff(minute, HIT_DATE, GetDate()) <= @HourSpan
            
) a
Order By HIT_DATE DESC
          
END