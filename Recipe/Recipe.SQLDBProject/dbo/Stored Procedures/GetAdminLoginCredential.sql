﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/29/2008>
-- Description:	<Return admin username and password for session variable>
-- =============================================
CREATE PROCEDURE [dbo].[GetAdminLoginCredential] 

@Username varchar(50)

AS
BEGIN

	SET NOCOUNT ON;

	SELECT uname, password from users where uname = @Username

END