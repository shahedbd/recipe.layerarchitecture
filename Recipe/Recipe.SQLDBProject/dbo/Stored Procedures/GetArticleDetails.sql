﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <6/5/2008>
-- Description:	<Returns article details>
-- =============================================
CREATE PROCEDURE [dbo].[GetArticleDetails] 

@AID int,
@Show int

AS
BEGIN

	SET NOCOUNT ON;
-- This is for the articledetail
if (@Show = 1)

Begin
Select a.ID,
       a.CAT_ID,
       b.CAT_NAME, 
       a.Title, 
       a.Post_Date, 
       a.Content, 
       a.Hits, 
       a.Author,
       a.Keyword,
       a.No_Rates, 
       CAST((1.0 * a.No_Rating/a.No_Rates) as decimal(2,1)) as Rates,
       a.Summary,
       a.Total_Comments        
       From Cooking_Article a Inner Join Cooking_Article_Category b
       ON a.CAT_ID = b.CAT_ID
       Where Show = 1 AND a.ID = @AID;

       -- Update article hit counter
       Update Cooking_Article set Hits = Hits + 1  where ID = @AID
End

else

Begin
-- This is for the Admin update recipe
Select a.ID,
       a.CAT_ID,
       b.CAT_NAME, 
       a.Title, 
       a.Post_Date, 
       a.Content,  
       a.Author,
       a.Keyword, 
       a.Summary       
       From Cooking_Article a Inner Join Cooking_Article_Category b
       ON a.CAT_ID = b.CAT_ID
       Where a.ID = @AID

End

END