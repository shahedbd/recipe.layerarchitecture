﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <7/4/2008>
-- Description:	<Delete Article Category>
-- =============================================
CREATE PROCEDURE AdminDeleteArticleCategory 

@CatId int

AS
BEGIN

	SET NOCOUNT ON;

    Delete From Cooking_Article_Category Where CAT_ID = @CatId

END