﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/17/2008>
-- Description:	<Get category for the homepage>
-- =============================================
CREATE PROCEDURE HomePage_GetCategory

AS
BEGIN

	SET NOCOUNT ON;

SELECT *,(SELECT COUNT (*)  
     FROM Recipes WHERE Recipes.CAT_ID = RECIPE_CAT.CAT_ID 
     AND LINK_APPROVED = 1) AS REC_COUNT 
     FROM RECIPE_CAT 
     ORDER BY CAT_TYPE ASC
END