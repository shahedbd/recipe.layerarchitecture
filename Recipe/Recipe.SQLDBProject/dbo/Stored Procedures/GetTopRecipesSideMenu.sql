﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/18/2008>
-- Description:	<Get the most popular recipes for the homepage and category page side menu.>
-- =============================================
CREATE PROCEDURE [dbo].[GetTopRecipesSideMenu] 

@CatId int = '',
@Top int

AS
BEGIN

SET ROWCOUNT @Top

-- If we're not in the category page, return the top 15 popular recipes.
If (@CatId = '')

    Begin

       SELECT ID,
              Name,
              HITS,
              Category 
       FROM Recipes 
       Where LINK_APPROVED = 1 
       Order By HITS DESC

    End

Else

-- If we're in the category page, return the top 15 popular recipes in that category.
Begin

       SELECT ID,
              Name,
              HITS,
              Category 
       FROM Recipes 
       Where CAT_ID = @CatId
       AND LINK_APPROVED = 1 
       Order By HITS DESC

End

    -- never forget to set it back to 0! 
    SET ROWCOUNT 0 

END