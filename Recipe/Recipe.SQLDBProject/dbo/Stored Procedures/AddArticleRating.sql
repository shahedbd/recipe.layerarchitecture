﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <6/6/2008>
-- Description:	<Add article rating>
-- =============================================
CREATE PROCEDURE AddArticleRating 

@ID int,
@Rating int

AS
BEGIN

	SET NOCOUNT ON;

    Update Cooking_Article Set No_Rating = No_Rating + @Rating, No_Rates = No_Rates + 1 WHERE ID = @ID

END