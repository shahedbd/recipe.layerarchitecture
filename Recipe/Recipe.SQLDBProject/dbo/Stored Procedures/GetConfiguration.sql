﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/21/2008>
-- Description:	<Show hide comments>
-- =============================================
CREATE PROCEDURE [dbo].[GetConfiguration]

AS
BEGIN

	SET NOCOUNT ON;

Select HideShowComment, Email, SmtpServer from Configuration

END