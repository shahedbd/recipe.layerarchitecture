﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/23/2008>
-- Description:	<Returns comments>
-- =============================================
CREATE PROCEDURE [dbo].[GetCommentsRecipeDetail]

@ID int

AS
BEGIN

	SET NOCOUNT ON;

      --- Get comments for the recipedeatil page
       SELECT ID, AUTHOR, EMAIL, COMMENTS, DATE From COMMENTS_RECIPE Where ID = @ID
       Order By DATE DESC

END