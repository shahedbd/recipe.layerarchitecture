﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/29/2008>
-- Description:	<Returns total comments count>
-- =============================================
CREATE PROCEDURE GetTotalCommentsCount

AS
BEGIN

	SET NOCOUNT ON;

    Select Count(*) From COMMENTS_RECIPE

END