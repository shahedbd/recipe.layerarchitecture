﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/29/08>
-- Description:	<Return recipe detail for approval admin review>
-- =============================================
CREATE PROCEDURE [dbo].[AdminRecipeApprovalReview]

@ID int

AS
BEGIN

	SET NOCOUNT ON;

-- Get recipe
Select ID,  
       Category,
       Date,
       Name,  
       HITS, 
       Ingredients,
       Instructions,
       Author,
       TOTAL_COMMENTS,
       LINK_APPROVED,
       HIT_DATE 
       FROM Recipes 
       WHERE ID = @ID

END