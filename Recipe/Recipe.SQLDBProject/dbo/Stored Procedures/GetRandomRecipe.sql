﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/21/2008>
-- Description:	<Return random recipe>
-- =============================================
CREATE PROCEDURE [dbo].[GetRandomRecipe]

@CatId int

AS
BEGIN

SET NOCOUNT ON;

If (@CatId = '')

Begin

-- If not category page, then return random from all categories.
Select ID, 
       CAT_ID, 
       Category, 
       Name,  
       HITS, 
       RATING,
       NO_RATES, 
       Cast((1.0 * RATING/NO_RATES) as decimal(2,1)) as Rates 
       FROM Recipes 
       WHERE LINK_APPROVED = 1
       Order By NEWID()

End

Else

Begin

-- if it is in the category page, then return random recipe from that category.
Select ID, 
       CAT_ID, 
       Category, 
       Name,  
       HITS, 
       RATING,
       NO_RATES, 
       Cast((1.0 * RATING/NO_RATES) as decimal(2,1)) as Rates 
       FROM Recipes 
       WHERE LINK_APPROVED = 1
       AND CAT_ID = @CatId
       Order By NEWID()

End

END