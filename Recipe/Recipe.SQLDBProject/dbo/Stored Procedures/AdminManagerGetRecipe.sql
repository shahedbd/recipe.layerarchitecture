﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/29/2008>
-- Description:	<Returns admin manager recipe with custom paging and filter>
-- =============================================
CREATE PROCEDURE [dbo].[AdminManagerGetRecipe]

@Letter varchar = '',
@CAT_ID int = '',
@Tab int = '',
@Find varchar(50) = '',
@Top int = '',
@Year int = '',
@Month int = '',
@RecipeImage int = '',
@LastViewed int = '',
@PageIndex int = 1,
@PageSize int = 20

AS
BEGIN

DECLARE @StartRow int
DECLARE @EndRow int

SET @StartRow = (@PageSize * (@PageIndex - 1))  + 1  
SET @EndRow = @PageSize * @PageIndex + 1

IF (@LastViewed <> '')
    BEGIN

    DECLARE @NumDays int

    IF (@LastViewed = 1)
        BEGIN
	        SET @NumDays = 1 -- 1 day
        END
    ELSE IF (@LastViewed = 2) 
        BEGIN
	        SET @NumDays = 2 -- 2 days
        END
    ELSE IF (@LastViewed = 3)
        BEGIN
	        SET @NumDays = 3 -- 3 days
        END
    ELSE IF (@LastViewed = 4)
        BEGIN
	        SET @NumDays = 4 -- 4 days
        END
    ELSE IF (@LastViewed = 5)
        BEGIN
	        SET @NumDays = 5 -- 5 days
        END
    ELSE IF (@LastViewed = 6)
        BEGIN
	        SET @NumDays = 6 -- 6 days
        END
    ELSE IF (@LastViewed = 7)
        BEGIN
	        SET @NumDays = 7 -- 1 week
        END
    ELSE IF (@LastViewed = 14)
        BEGIN
	       SET @NumDays = 14 -- 2 weeks
        END
    ELSE IF (@LastViewed = 30)
        BEGIN
	       SET @NumDays = 30 -- 1 month
        END
    ELSE
        BEGIN
	        SET @NumDays = 1 -- default to 1 day
        END
END

SET NOCOUNT ON;

-- No filter applied - show default result set
If (@Letter = '' AND @CAT_ID = '' AND @Find = '' AND @Tab = '' AND @Top = '' 
AND @Year = '' AND @Month = '' AND @RecipeImage = '' AND @LastViewed = '')
   BEGIN
WITH Recipe AS
(
    SELECT
       ROW_NUMBER() OVER 
         (
            ORDER BY ID
         ) AS RowNumber, 
          ID, 
          CAT_ID, 
          Category, 
          Name, 
          Author, 
          Date, 
          HITS,
          RecipeImage,
          (SELECT Count(*) FROM Recipes) AS RCount
      FROM Recipes 
)
-- Statement that executes the CTE
SELECT a.*
FROM
      Recipe a
WHERE
      a.RowNumber BETWEEN @StartRow AND @EndRow - 1
ORDER BY
      a.RowNumber
END 

ELSE

--Search and Alphabet letter filter
IF (@Find <> '' OR @Letter <> '')
   BEGIN
WITH Recipe AS
(
    SELECT
       ROW_NUMBER() OVER 
         (
            ORDER BY ID
         ) AS RowNumber, 
           ID, 
           CAT_ID, 
           Category, 
           Name, 
           Author, 
           Date, 
           HITS,
           RecipeImage,
           (SELECT Count(*) FROM Recipes WHERE Name LIKE '%' + COALESCE(@Find, Name) + '%' 
      AND Name LIKE COALESCE(@Letter, Name) + '%') AS RCount
      FROM Recipes 
      WHERE Name LIKE '%' + COALESCE(@Find, Name) + '%' 
      AND Name LIKE COALESCE(@Letter, Name) + '%'
)
-- Statement that executes the CTE
SELECT a.*
FROM
      Recipe a
WHERE
      a.RowNumber BETWEEN @StartRow AND @EndRow - 1
ORDER BY
      a.RowNumber
  END

--Category filter
IF (@CAT_ID <> '')
   BEGIN
WITH Recipe AS
(
    SELECT
       ROW_NUMBER() OVER 
         (
            ORDER BY ID
         ) AS RowNumber, 
           ID, 
           CAT_ID, 
           Category, 
           Name, 
           Author, 
           Date, 
           HITS,
           RecipeImage,
           (SELECT Count(*) FROM Recipes WHERE CAT_ID = @CAT_ID) AS RCount
       FROM Recipes 
       WHERE CAT_ID = @CAT_ID
)
-- Statement that executes the CTE
SELECT a.*
FROM
      Recipe a
WHERE
      a.RowNumber BETWEEN @StartRow AND @EndRow - 1
ORDER BY
      a.RowNumber
   END

--Waiting for approval/Unapproved recipe filter
IF (@Tab <> '')
   BEGIN
WITH Recipe AS
(
    SELECT
       ROW_NUMBER() OVER 
         (
            ORDER BY ID
         ) AS RowNumber, 
           ID, 
           CAT_ID, 
           Category, 
           Name, 
           Author, 
           Date, 
           HITS,
           RecipeImage,
           (SELECT Count(*) FROM Recipes WHERE LINK_APPROVED = 0) AS RCount
      FROM Recipes 
      WHERE LINK_APPROVED = 0
)
-- Statement that executes the CTE
SELECT a.*
FROM
      Recipe a
WHERE
      a.RowNumber BETWEEN @StartRow AND @EndRow - 1
ORDER BY
      a.RowNumber
  END

-- 100 Most popular recipe filter
IF (@Top <> '')
   BEGIN
WITH Recipe AS
(
    SELECT TOP 100
       ROW_NUMBER() OVER 
         (
            ORDER BY HITS DESC
         ) AS RowNumber, 
                   ID, 
                   CAT_ID, 
                   Category, 
                   Name, 
                   Author, 
                   Date, 
                   HITS,
                   RecipeImage,
             (SELECT TOP 100 Count(*) FROM Recipes) AS RCount
             FROM Recipes 
             ORDER BY Hits DESC
)
-- Statement that executes the CTE
SELECT a.*
FROM
      Recipe a
WHERE
      a.RowNumber BETWEEN @StartRow AND @EndRow - 1
ORDER BY
      a.RowNumber
END

-- Month search
IF (@Year <> '' AND @Month <> '')
   BEGIN
WITH Recipe AS
(
    SELECT
       ROW_NUMBER() OVER 
         (
            ORDER BY ID
         ) AS RowNumber, 
           ID, 
           CAT_ID, 
           Category, 
           Name, 
           Author, 
           Date, 
           HITS,
           RecipeImage,
           (SELECT Count(*) FROM Recipes WHERE (DATEPART([year], Date) = @Year) 
       AND (DATEPART([month], Date) = @Month)) AS RCount
       FROM Recipes 
       WHERE (DATEPART([year], Date) = @Year) 
       AND (DATEPART([month], Date) = @Month)
)
-- Statement that executes the CTE
SELECT a.*
FROM
      Recipe a
WHERE
      a.RowNumber BETWEEN @StartRow AND @EndRow - 1
ORDER BY
      a.RowNumber
   END

-- Recipe Image Filter
IF (@RecipeImage <> '')
   BEGIN
WITH Recipe AS
(
    SELECT
       ROW_NUMBER() OVER 
         (
            ORDER BY ID
         ) AS RowNumber, 
           ID, 
           CAT_ID, 
           Category, 
           Name, 
           Author, 
           Date, 
           HITS,
           RecipeImage,
           (SELECT Count(*) FROM Recipes WHERE RecipeImage IS NOT NULL AND LEN(RecipeImage) > 1) AS RCount
       FROM Recipes 
       WHERE RecipeImage IS NOT NULL AND LEN(RecipeImage) > 1
)
-- Statement that executes the CTE
SELECT a.*
FROM
      Recipe a
WHERE
      a.RowNumber BETWEEN @StartRow AND @EndRow - 1
ORDER BY
      a.RowNumber
   END

-- Show last viewed recipe by days, week and month
IF (@LastViewed <> '')
 BEGIN	  
WITH Recipe AS
(
    SELECT
       ROW_NUMBER() OVER 
         (
            ORDER BY ID
         ) AS RowNumber, 
           ID, 
           CAT_ID, 
           Category, 
           Name, 
           Author, 
           Date, 
           HITS,
           RecipeImage,
           HIT_DATE,
           (SELECT Count(*) FROM Recipes Where HIT_DATE >= DATEADD(day, - @NumDays, GETDATE())) AS RCount 
      FROM Recipes 
      Where HIT_DATE >= DATEADD(day, - @NumDays, GETDATE())
)
-- Statement that executes the CTE
SELECT a.*
FROM
      Recipe a
WHERE
      a.RowNumber BETWEEN @StartRow AND @EndRow - 1
ORDER BY
      a.RowNumber
  END

END