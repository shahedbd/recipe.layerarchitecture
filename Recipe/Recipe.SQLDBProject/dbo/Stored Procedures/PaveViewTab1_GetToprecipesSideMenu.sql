﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/17/2008>
-- Description:	<Get the top 15 most popular recipe in the pageview.aspx where querstring tab = 1>
-- =============================================
CREATE PROCEDURE [dbo].[PaveViewTab1_GetToprecipesSideMenu]
	
@CategoryID int,
@Tab int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

If (@Tab = 1)
 
Begin

SELECT TOP 15 ID,
                 Name,
                 HITS,
                 Category 
                 FROM Recipes WHERE CAT_ID = @CategoryID 
                 AND LINK_APPROVED = 1 
                 Order By HITS DESC

End

Else

 Begin

SELECT Top 15 ID,
           Name,
           HITS,
           Category FROM Recipes 
           Where LINK_APPROVED = 1 
           Order By HITS DESC

End

END