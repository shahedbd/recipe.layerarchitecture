﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <6/12/2008>
-- Description:	<Get Recipe Details for update>
-- =============================================
CREATE PROCEDURE GetRecipeDetailsForUpdate 

@ID int

AS
BEGIN

	SET NOCOUNT ON;

-- Get recipe for admin update
Select ID, 
       Name,  
       Author,
       HITS, 
       Ingredients,
       Instructions
       FROM Recipes 
       WHERE ID = @ID

End