﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <6/5/2008>
-- Description:	<Update article>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateArticle]

@AID int,
@Title varchar(200),
@Content ntext,
@Author varchar(50),
@CAT_ID int,
@Keyword varchar(255),
@Summary ntext

AS

declare @ErrorCode int

Begin

Update Cooking_Article Set 
                       Title = @Title,
                       Content = @Content,
                       Author = @Author,
                       CAT_ID = @CAT_ID,
                       Keyword = @Keyword,
                       Summary = @Summary,
                       Update_date = getdate()
                       Where ID = @AID

set @ErrorCode = @@error
		
End

return @ErrorCode