﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/23/2008>
-- Description:	<Returns homepage total category count>
-- =============================================
CREATE PROCEDURE GetHompageTotalCategoryCount 

AS
BEGIN

	SET NOCOUNT ON;

    SELECT COUNT(*) FROM RECIPE_CAT

END