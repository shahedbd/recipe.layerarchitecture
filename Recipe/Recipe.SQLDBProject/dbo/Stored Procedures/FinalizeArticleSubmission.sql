﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <6/6/2008>
-- Description:	<Finlize article submission>
-- =============================================
CREATE PROCEDURE FinalizeArticleSubmission

@ID int

AS
BEGIN

	SET NOCOUNT ON;

	UPDATE Cooking_Article SET Show = 1
    Where ID = @ID

END