﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/21/2008>
-- Description:	<Return recipe homepage category main course>
-- =============================================
CREATE PROCEDURE GetHomePageCategoryMainCourseRecipe 

AS
BEGIN

	SET NOCOUNT ON;

     SELECT *,(SELECT COUNT (*)  
     FROM Recipes WHERE Recipes.CAT_ID = RECIPE_CAT.CAT_ID 
     AND LINK_APPROVED = 1) AS REC_COUNT 
     FROM RECIPE_CAT WHERE GROUPCAT = 1
     ORDER BY CAT_TYPE ASC

END