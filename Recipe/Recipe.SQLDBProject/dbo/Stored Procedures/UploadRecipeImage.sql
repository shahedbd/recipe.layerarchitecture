﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <9/5/2008>
-- Description:	<Recipe Image Upload>
-- =============================================
CREATE PROCEDURE [dbo].[UploadRecipeImage] 

@FileName varchar(50)

AS

declare @ErrorCode int

set @ErrorCode = @@error

BEGIN TRANSACTION

if ( @ErrorCode = 0 )

Begin

-- Insert
INSERT INTO RecipeImage ([FileName])
	VALUES(
		@FileName
		)
End

if ( @ErrorCode = 0 )
	COMMIT TRANSACTION
else
	ROLLBACK TRANSACTION

return @ErrorCode