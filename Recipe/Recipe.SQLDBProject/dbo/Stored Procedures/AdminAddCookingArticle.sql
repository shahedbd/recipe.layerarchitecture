﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <6/5/2008>
-- Description:	<Add cooking article>
-- =============================================
CREATE PROCEDURE [dbo].[AdminAddCookingArticle] 

@Title varchar(200),
@Content ntext,
@Author varchar(50),
@CAT_ID int,
@Keyword varchar(255),
@Summary ntext

AS

declare @ErrorCode int

set @ErrorCode = @@error

BEGIN TRANSACTION

if ( @ErrorCode = 0 )

Begin

INSERT INTO Cooking_Article (Title,Content,Author,CAT_ID,Keyword,Summary)
	VALUES(
		@Title,
		@Content,
		@Author,
        @CAT_ID,
        @Keyword,
        @Summary
		)
End

if ( @ErrorCode = 0 )
	COMMIT TRANSACTION
else
	ROLLBACK TRANSACTION

return @ErrorCode