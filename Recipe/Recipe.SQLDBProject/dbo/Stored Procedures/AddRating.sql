﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/23/2008>
-- Description:	<Insert rating>
-- =============================================
CREATE PROCEDURE AddRating

@ID int,
@Rating int

AS
BEGIN

	SET NOCOUNT ON;

    Update Recipes  SET RATING = RATING + @Rating, NO_RATES = NO_RATES + 1 WHERE ID = @ID

END