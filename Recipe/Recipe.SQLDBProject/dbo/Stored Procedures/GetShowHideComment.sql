﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <5/31/2008>
-- Description:	<Return show / hide comment value>
-- =============================================
CREATE PROCEDURE GetShowHideComment 

AS
BEGIN

	SET NOCOUNT ON;

    Select HideShowComment From Configuration

END