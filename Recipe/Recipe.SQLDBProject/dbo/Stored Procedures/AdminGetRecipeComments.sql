﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <7/3/2008>
-- Description:	<Admin display comments in datagrid>
-- =============================================
CREATE PROCEDURE [dbo].[AdminGetRecipeComments] 

AS
BEGIN

	SET NOCOUNT ON;

    Select COM_ID, ID, AUTHOR, EMAIL, COMMENTS, DATE FROM COMMENTS_RECIPE Order By DATE DESC

END