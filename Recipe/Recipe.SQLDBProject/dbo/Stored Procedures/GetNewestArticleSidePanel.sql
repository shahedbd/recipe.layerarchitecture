﻿-- =============================================
-- Author:		<Dexter Zafra>
-- Create date: <6/6/2008>
-- Description:	<Returns 10 newest cooking articles in the right side panel.>
-- =============================================
CREATE PROCEDURE [dbo].[GetNewestArticleSidePanel] 

@Top int

AS
BEGIN

	SET ROWCOUNT @Top

    Select a.ID, 
           a.CAT_ID,
           a.Title,
           a.Hits,
           a.Post_Date,
           b.CAT_NAME
       From Cooking_Article a Inner Join Cooking_Article_Category b
       ON a.CAT_ID = b.CAT_ID
       Where a.Show = 1
       Order By a.Post_Date DESC

    -- never forget to set it back to 0! 
    SET ROWCOUNT 0 

END