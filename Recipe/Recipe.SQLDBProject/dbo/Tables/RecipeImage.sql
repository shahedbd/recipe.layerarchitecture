﻿CREATE TABLE [dbo].[RecipeImage] (
    [ID]       INT          IDENTITY (1, 1) NOT NULL,
    [FileName] VARCHAR (50) NOT NULL
);

