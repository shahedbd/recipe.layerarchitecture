﻿CREATE TABLE [dbo].[Configuration] (
    [HideShowComment] INT          NOT NULL,
    [Email]           VARCHAR (50) NOT NULL,
    [SmtpServer]      VARCHAR (50) NOT NULL
);

