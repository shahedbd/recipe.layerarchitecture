﻿CREATE TABLE [dbo].[ExceptionLog] (
    [ID]        INT            IDENTITY (1, 1) NOT NULL,
    [URL]       VARCHAR (100)  NULL,
    [Exception] VARCHAR (1000) NULL,
    [Date]      DATETIME       CONSTRAINT [DF_ExceptionLog_Date] DEFAULT (CONVERT([datetime],CONVERT([varchar],getdate(),(1)),(1))) NOT NULL,
    CONSTRAINT [PK_ExceptionLog] PRIMARY KEY CLUSTERED ([ID] ASC)
);

