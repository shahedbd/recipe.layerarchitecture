﻿CREATE TABLE [dbo].[Cooking_Article] (
    [ID]             INT            IDENTITY (1, 1) NOT NULL,
    [CAT_ID]         INT            NULL,
    [Title]          NVARCHAR (100) NOT NULL,
    [Post_Date]      DATETIME       CONSTRAINT [DF_Article_Post_Date] DEFAULT (CONVERT([datetime],CONVERT([varchar],getdate(),(1)),(1))) NULL,
    [Content]        NTEXT          NULL,
    [Hits]           INT            CONSTRAINT [DF_Cooking_Article_Hits] DEFAULT ((1)) NULL,
    [Author]         NVARCHAR (100) NULL,
    [Keyword]        NVARCHAR (255) NULL,
    [Show]           INT            CONSTRAINT [DF_Cooking_Article_Show] DEFAULT ((0)) NULL,
    [No_Rating]      INT            CONSTRAINT [DF_Article_No_Rating] DEFAULT ((5)) NULL,
    [No_Rates]       INT            CONSTRAINT [DF_Article_No_Rates] DEFAULT ((1)) NULL,
    [Summary]        NTEXT          NULL,
    [Total_Comments] INT            CONSTRAINT [DF_Cooking_Article_Total_Comments] DEFAULT ((0)) NULL,
    [Update_date]    DATETIME       CONSTRAINT [DF_Cooking_Article_Updated] DEFAULT (CONVERT([datetime],CONVERT([varchar],getdate(),(1)),(1))) NULL,
    CONSTRAINT [PK_Article] PRIMARY KEY CLUSTERED ([ID] ASC)
);

