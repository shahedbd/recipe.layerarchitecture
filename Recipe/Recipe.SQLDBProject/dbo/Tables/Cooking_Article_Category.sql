﻿CREATE TABLE [dbo].[Cooking_Article_Category] (
    [CAT_ID]   INT            IDENTITY (1, 1) NOT NULL,
    [CAT_NAME] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_Article_Category] PRIMARY KEY CLUSTERED ([CAT_ID] ASC)
);

