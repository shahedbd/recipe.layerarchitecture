﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Recipe.Client.Startup))]
namespace Recipe.Client
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
